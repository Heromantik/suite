var MailListener = require("mail-listener2");
var MongoClient = require("mongodb").MongoClient;
var sendMail = require("./outbox");
var express = require("express");
var bodyParser = require("body-parser");

const app = express();
var db;
let admin;
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

app.use(async (req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  next();
});
app.use(express.static(__dirname + '/dist/'));
app.get(/.*/, (req, res) => res.sendFile(__dirname + "/dist/index.html"));
if (process.env.NODE_ENV === "production") {
  console.log("production")
} else {

  console.log("not production")
}

const port = 80;


app.post("/about", function (req, res) {
  db.collection("urltoken")
    .findOne({
      key: req.body.key
    })
    .then(result => {
      if (result) {
        let tokendata = {
          key: result.key,
          email: result.email,
          usertype: result.usertype
        };
        db.collection("students")
          .findOne({
            email: tokendata.email
          }, {
            projection: {
              _id: 0
            }
          })
          .then(data => {
            if (data) {
              let userdata = {
                user: data,
                userattr: tokendata
              };
              res.send(userdata);
            } else {
              console.log("3 No document matches the provided query.");
              res.sendStatus(500);
            }
          });
      } else {
        db.collection("adminlist")
          .findOne({
            key: req.body.key
          })
          .then(result => {
            if (result) {
              let tokendata = {
                key: result.key,
                email: result.email,
                usertype: result.usertype
              };
              db.collection("group")
                .find()
                .project({
                  _id: 0
                })
                .toArray()
                .then(groups => {
                  if (groups) {
                    let grps = groups.map(item => (item = item.group));
                    db.collection("students")
                      .find()
                      .project({
                        _id: 0,
                        balance: 0,
                        orders: 0
                      })
                      .toArray()
                      .then(stud => {
                        let studentsdata = [];
                        grps.forEach(element => {
                          let arr = {};
                          let students = [];
                          arr["group"] = element;
                          stud.forEach(stud => {
                            if (stud.group == element) {
                              students.push(stud);
                            }
                          });
                          let arrstudents = students.sort((a, b) => a.rating - b
                            .rating).reverse();
                          arr["students"] = arrstudents;
                          studentsdata.push(arr);
                        });
                        db.collection("adminhistory")
                          .find()
                          .project({
                            _id: 0
                          })
                          .toArray()
                          .then(hist => {
                            let data = {
                              user: {
                                students: studentsdata,
                                history: hist
                              },
                              userattr: tokendata
                            };
                            res.send(data);
                          });
                      });
                  } else {
                    console.log("1 No document matches the provided query.");
                    res.sendStatus(500);
                  }
                });
            }
          });
      }
    })
    .catch(err => console.error(`Failed to find document: ${err}`));
});
// app.post("/about/lk/admin", function (req, res) {
// });

// app.post("/about/lk/student", function (req, res) {
// });

app.post("/shop", function (req, res) {
  db.collection("shop")
    .find()
    .project({
      _id: 0
    })
    .toArray()
    .then(result => {
      if (result) {
        res.send(result);
      } else {
        console.log("No document matches the provided query.");
        res.sendStatus(500);
      }
    })
    .catch(err => console.error(`Failed to find document: ${err}`));
});

app.post("/liderboard", function (req, res) {
  db.collection("group")
    .find()
    .project({
      _id: 0
    })
    .toArray()
    .then(groups => {
      if (groups) {
        let grps = groups.map(item => (item = item.group));
        db.collection("students")
          .find()
          .project({
            _id: 0,
            balance: 0,
            email: 0,
            orders: 0
          })
          .toArray()
          .then(studs => {
            let data = [];
            grps.forEach(element => {
              let arr = {};
              let students = [];
              arr["group"] = element;
              studs.forEach(stud => {
                if (stud.group == element) {
                  students.push(stud);
                }
              });
              let arrstudents = students
                .sort((a, b) => a.rating - b.rating)
                .reverse()
                .slice(0, 10);
              arr["students"] = arrstudents;
              data.push(arr);
            });
            let resdata = {
              groupsdata: data,
              groupslist: grps
            };
            res.send(resdata);
          });
      } else {
        console.log("No document matches the provided query.");
        res.sendStatus(500);
      }
    })
    .catch(err => console.error(`Failed to find document: ${err}`));
});

MongoClient.connect(
  "mongodb://localhost:27017/brs", {
    useUnifiedTopology: true
  },
  function (err, database) {
    if (err) {
      return console.log(err);
    }
    db = database.db("brs");
    app.listen(port, function () {
      console.log(`Server started on port ${port}`);
      console.log(__dirname)
    });
    db.collection("adminlist")
      .find()
      .project({
        _id: 0,
        email: 1
      })
      .toArray()
      .then(items => {
        admin = items;
      })
      .catch(err => console.error(`Failed to find documents: ${err}`));
  }
);

async function checkUserUrl(email) {
  if (admin.find(item => item.email == email)) {
    try {
      let res = await db.collection("adminlist").findOne({
        email: email
      });
      return res;
    } catch (err) {
      console.log(err);
    }
  } else {
    try {
      let res = await db.collection("urltoken").findOne({
        email: email
      });
      return res;
    } catch (err) {
      console.log(err);
    }
  }
}

async function updateUserUrl(email) {
  const dataUrl = setUrl(email);
  let date = Date.now() + 600000;
  if (admin.find(item => item.email == email)) {
    try {
      let res = await db.collection("adminlist").replaceOne({
        email: email
      }, {
        $set: {
          url: dataUrl.url,
          key: dataUrl.key,
          date: date
        }
      });
      console.log("update admin");
      return res;
    } catch (err) {
      console.log(err);
    }
  } else {
    try {
      let res = await db.collection("urltoken").replaceOne({
        email: email
      }, {
        $set: {
          url: dataUrl.url,
          key: dataUrl.key,
          date: date
        }
      });
      console.log("update stud");
      return res;
    } catch (err) {
      console.log(err);
    }
  }
}

async function setNewUserUrl(email) {
  const dataUrl = setUrl(email);
  let date = Date.now() + 600000;
  let tokenRequest = {
    email: email,
    url: dataUrl.url,
    key: dataUrl.key,
    date: date
  };
  if (admin.find(item => item.email == email)) {
    tokenRequest["usertype"] = "admin";
    try {
      let res = db.collection("adminlist").insertOne(tokenRequest);
      return res;
    } catch (err) {
      console.log(err);
    }
  } else {
    tokenRequest["usertype"] = "student";
    try {
      let res = db.collection("urltoken").insertOne(tokenRequest);
      return res;
    } catch (err) {
      console.log(err);
    }
  }
}

function setUrl(email) {
  const secretkey = "sN9bfMAN5tzjN56gCYs7PZ0M6uIvnt";
  let date = Date.now();
  let key = Buffer.from(email + date + secretkey)
    .toString("base64")
    .split("")
    .sort(function () {
      return Math.random() - 0.5;
    })
    .join("");
  let url = "http://localhost:8080/about?lk=" + key;
  return {
    url,
    key
  };
}

var mailListener = new MailListener({
  username: "i5brs@voenmeh.ru",
  password: "ushahW9n",
  host: "mail.voenmeh.ru",
  port: 993,
  tls: true,
  connTimeout: 10000,
  authTimeout: 5000,
  debug: console.log,
  tlsOptions: {
    rejectUnauthorized: false
  },
  mailbox: "INBOX",
  searchFilter: ["UNSEEN"],
  markSeen: true,
  fetchUnreadOnStart: true,
  mailParserOptions: {
    streamAttachments: true
  },
  attachments: true,
  attachmentOptions: {
    directory: "attachments/"
  }
});

mailListener.start();

// stop listening

mailListener.on("server:connected", function () {
  console.log("imapConnected");
});

mailListener.on("server:disconnected", function () {
  console.log("imapDisconnected");
  mailListener.stop();
  mailListener.start();
});

mailListener.on("error", function (err) {
  console.log(err);
});

mailListener.on("mail", function (mail, seqno, attributes) {
  console.log(mail, seqno, attributes);

  if (mail.from[0].address.match(/@voenmeh.ru/)) {
    let msg = mail.text.replace(/[^A-Za-zА-Яа-яЁё]/g, "");
    let email = mail.from[0].address;
    if (msg) {
      if (msg.match(/вход/)) {
        console.log("yes", email);
        (async () => {
          let res = await checkUserUrl(email);
          if (res) {
            console.log("in db");
            let dateR = Date.now();
            if (dateR > res.date) {
              console.log(res, "can update");
              let upd = await updateUserUrl(email);
              console.log(upd.ops[0].$set.url, "update");
              sendMail(email, upd.ops[0].$set.url);
            } else {
              console.log(res, "Already created");
            }
          } else {
            (async () => {
              let res = await setNewUserUrl(email);
              console.log(res.ops, "create new");
              let url = res.ops[0].url;
              sendMail(email, url);
              console.log(res.ops[0].url, "done");
            })();
          }
        })();
      } else {
        console.log("Not found command");
      }
    } else {
      console.log("Empty mail");
    }
  } else {
    console.log("Wrong mail");
  }
});
