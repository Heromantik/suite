var docxParser = require('docx-parser');
var fs = require("fs")
docxParser.parseDocx("stud1.docx", function (res) {
  let data = res.split('\n')
  let out = []
  let gr = [];
  let igr = [];
  data.forEach(function (el, i, data) {
    if (el === "Группа:") {
      igr.push(i)
      let trimdata = (data[i + 1]).trim()
      gr.push(trimdata);
    }
  });
  igr.push(data.length);
  gr.forEach(function (group) {
    let data1 = data.slice(igr[0], igr[1])
    igr.splice(0, 1)

    let inx = 1;

    let study = [];
    data1.forEach(function (el, i) {
      let studpos = inx + '.';

      if (el === studpos) {
        inx++
        study.push({
          lastname: data1[i + 1],
          name: data1[i + 2],
          secondname: data1[i + 3],
          stud: data1[i + 4]
        })
      }
    });
    let out1 = {
      group: group,
      students: study
    }
    out.push(out1)
    study = [];

  })
  fs.writeFile("out.json", JSON.stringify(out), function (err) {
    if (err) throw err;
  });
})
